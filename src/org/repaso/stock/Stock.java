package org.repaso.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;
import org.repaso.excepciones.NoStockException;
import org.repaso.excepciones.NonValidCodeException;
import org.repaso.producto.Helado;

/**
 * Control de <b>Stock</b> de la heladeria, almacena los gustos de Helado
 * {@link org.repaso.producto.Helado}
 *
 */
public class Stock {
	
	private static HashMap<Integer, Helado> helados;
	
	public static void init() {
		helados = new HashMap<Integer, Helado>();
	}
	
	/**
	 * Agrega un helado al Stock
	 * @param codigo	&emsp;(<b>Integer</b>) ID del helado
	 * @param helado	&emsp;(<b>Helado</b>) {@link org.repaso.producto.Helado}
	 */
	public static void agregar(int codigo, Helado helado) {
		helados.put(codigo, helado);
	}
	
	/**
	 * <p>Lista los codigos de los helados en Stock</p>
	 * @deprecated los codigos son secuenciales (0 -> n)
	 */
	@Deprecated
	public static void listarCodigos() {
		for (Map.Entry<Integer, Helado> entry : helados.entrySet()) {
		    System.out.println("Codigo:" + entry.getKey() + ", Gusto: " + entry.getValue().getNombre());
		}
	}
	
	/**
	 * <p>Retorna un String correspondiente a la receta del gusto de helado</p>
	 * @param codigo	&emsp;(<b>Integer</b>) ID del Helado
	 * @return String 	Receta correspondiente al gusto de helado
	 * @throws NonValidCodeException
	 */
	public static String obtenerReceta(int codigo) throws NonValidCodeException {
		for (Map.Entry<Integer, Helado> entry : helados.entrySet()) {
			if(entry.getKey().compareTo(codigo) == 0) {
				return "Receta del helado [" + entry.getValue().getNombre() + "] " + entry.getValue().getReceta();
			}
		}	
		throw new NonValidCodeException(codigo);
	}
	
	/**
	 * <p>Retorna los helados en Stock que esten por debajo de la cantidad que pasamos por parametro</p>
	 * @param cantidad 		&emsp;(<b>Double</b>)Cantidad MAXIMA de kg en los gustos de helado
	 * @return ArrayList	Arreglo de Helados en Stock
	 */
	public static ArrayList<Helado> listar(double cantidad) {
		
		ArrayList<Helado> listado = new ArrayList<Helado>();
		
		for (Map.Entry<Integer, Helado> entry : helados.entrySet()) {
			if(cantidad > entry.getValue().getCantidad()) {
				listado.add(entry.getValue());
			}
		}
		
		return listado;
	}
		
	/**
	 * <p>Vende un gusto de helado almacenado en el stock</p>
	 * @param codigo	&emsp;(<b>Integer</b>) ID del helado
	 * @param peso		&emsp;(<b>Double</b>) Cantidad que se utiliza de un determinado sabor
	 * @param gustos	&emsp;(<b>Integer</b>) Cantidad de sabores que existen en la venta (para calcular la porcion)
	 * @return Helado 	&emsp;Retorna el helado para utilizar los datos del mismo
	 * @throws NonValidCodeException
	 * @throws NoStockException
	 */
	public static Helado vender(int codigo, double peso, int gustos) throws NonValidCodeException, NoStockException {
		for (Map.Entry<Integer, Helado> entry : helados.entrySet()) {
			if(entry.getKey().compareTo(codigo) == 0) {
				if(entry.getValue().getCantidad() - (peso / gustos) < 0) {
					throw new NoStockException(entry.getValue().getNombre());
				} else {
					entry.getValue().vender(peso / gustos);
				}
				return entry.getValue();
			}
		}
		throw new NonValidCodeException(codigo); // En caso de no encontrar el codigo devuelve una excepcion
	}
	
	/**
	 * <p>Parsea los datos de cada helado a un JSONObject y lo retorna</p>
	 * @return JSONObject ({@link org.json.JSONObject}
	 */
	public static JSONObject exportarJSON() {
		JSONObject ret = new JSONObject();
		JSONArray heladosArray = new JSONArray();
		
		for (Map.Entry<Integer, Helado> helado : helados.entrySet()) {
	        
	        // Creamos el objeto que hace referencia al sabor de helado
	        JSONObject object = new JSONObject();
	        object.put("sabor", helado.getValue().getNombre());
	        object.put("cantidad", helado.getValue().getCantidad());
	        
	        /*
	         * Receta
	         */
	        JSONArray recetasArray = new JSONArray();
	        for (Map.Entry<String, Integer> receta : helado.getValue().getReceta().entrySet()) {
		        
		        // Creamos un objeto para cada una de las recetas
		        JSONObject recetaObject = new JSONObject();
		        recetaObject.put("ingrediente", receta.getKey());
		        recetaObject.put("cantidad", receta.getValue());
		        
		        // Agregamos al arreglo de receta (del helado)
		        recetasArray.put(recetaObject);
		    }
	        
	        // Agregamos todas las recetas que cargamos al objecto helado
	        object.put("receta", recetasArray);
	        
	        // Agregamos el objeto 'helado' al array con la receta correspondiente
		    heladosArray.put(object);
	    }
	    
	    ret.put("helados", heladosArray);
		
		return ret;

	}

}

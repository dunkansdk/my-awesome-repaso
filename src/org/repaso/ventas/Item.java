package org.repaso.ventas;

import java.util.ArrayList;

import org.repaso.excepciones.InvalidEnvaseException;
import org.repaso.excepciones.MaximumTastesException;
import org.repaso.excepciones.NoStockException;
import org.repaso.excepciones.NonValidCodeException;
import org.repaso.producto.Envase;
import org.repaso.producto.Helado;
import org.repaso.stock.Stock;

/**
 * Items incluidos en una venta, determinando:
 * @see {@link org.repaso.producto.Envase}
 * @see {@link org.repaso.producto.Helado}
 */
public class Item {
	
	private Envase envase;
	private ArrayList<String> gustos;
	
	/**
	 * Crea un item de venta
	 * @param envase	&emsp;(<b>Envase</b>) {@link org.repaso.producto.Envase}
	 * @param codigos	&emsp;(<b>Integer[n]</b>) Cantidad n de codigos (Sabores de helado)
	 * @see {@link org.repaso.ventas.Venta}
	 * @throws InvalidEnvaseException
	 */
	public Item(Envase envase, Integer ... codigos) throws InvalidEnvaseException {
		
		this.envase = envase;
		gustos = new ArrayList<String>();
		
		for(int code : codigos) {
			try {
				agregar(Stock.vender(code, envase.getPeso(), codigos.length));
			} catch (MaximumTastesException | NonValidCodeException | NoStockException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * <p>Agrega un gusto de helado al envase asignado en el item</p>
	 * @param helado	&emsp;(<b>Helado</b>) {@link org.repaso.producto.Helado}
	 * @see {@link org.repaso.producto.Envase}
	 * @throws MaximumTastesException
	 */
	public void agregar(Helado helado) throws MaximumTastesException {
		if(gustos.size() < envase.getCantidad()) 
			gustos.add(helado.getNombre());
		else
			throw new MaximumTastesException();
	}
	
	public ArrayList<String> getGustos() {
		return gustos;
	}
	
	public Envase getEnvase() {
		return envase;
	}

	@Override
	public String toString() {
		return "Item [envase=" + envase + ", gustos=" + gustos + "]";
	}
	
}

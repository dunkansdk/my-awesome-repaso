package org.repaso.ventas;

import java.util.ArrayList;

/**
 * Genera una venta a partir de los items incluidos
 * {@link org.repaso.ventas.Item}
 */
public class Venta {
	
	private double total;
	private ArrayList<Item> items;
	
	/**
	 * <p>Items que se van a vender a un cliente</p>
	 * @see {@link org.repaso.ventas.Item}
	 * @param item
	 */
	public Venta(Item ... item) {
		
		items = new ArrayList<Item>();
		
		for(Item i : item) {
			// Si el gusto no esta en stock, simplemente no lo agrega, onda, "Ya fue, dame todo de vainilla"
			if(i.getGustos().size() != 0) {
				this.items.add(i);
				total += i.getEnvase().getPrecio();
			}
		}
	}
	
	public double getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return "Venta [total=" + total + ", items=" + items + "]";
	}

}

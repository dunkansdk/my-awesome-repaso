package org.repaso.excepciones;

/**
 * <p>No hay stock del helado especificado</p>
 * @param message	&emsp;(<b>String</b>) nombre del helado
 */
public class NoStockException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoStockException(String message) {
		super("No hay stock del helado especificado: " + message);
	}
}

package org.repaso.excepciones;

/**
 * <p>El envase no permite agregar mas gustos de helado</p>
 * @see {@link org.repaso.producto.Envase#getCantidad()}
 */
public class MaximumTastesException extends Exception {

	private static final long serialVersionUID = 1L;

	public MaximumTastesException() {
		super("El envase no permite mas sabores");
	}

}

package org.repaso.excepciones;

/**
 * <p>Codigo del helado inexistente</p>
 * @param code	&emsp;(<b>Integer</b>) ID ingresado
 */
public class NonValidCodeException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NonValidCodeException(int code) {
		super("No existe el codigo del helado: " + code);
	}

}

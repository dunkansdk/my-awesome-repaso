package org.repaso.excepciones;

/**
 * El envase no existe / no fue creado
 * @param message	&emsp;(<b>String</b>) etiqueta del envase
 */
public class InvalidEnvaseException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidEnvaseException(String message) {
		super("El envase '" + message + "' no existe.");
	}

}

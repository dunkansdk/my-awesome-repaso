package org.repaso.heladeria;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.repaso.excepciones.InvalidEnvaseException;
import org.repaso.producto.Envase;

/**
 * Controlador de los envases
 * @see {@link org.repaso.producto.Envase}
 */
public class EnvaseController {
	
	private static HashMap<String, Envase> envases;

	public static void init() {
		envases = new HashMap<String, Envase>();
	}

	/**
	 * <p>Agrega un envase a la lista</p>
	 * @param etiqueta	&emsp;(<b>String</b>) etiqueta identificadora
	 * @param envase	&emsp;(<b>Integer</b>) datos del envase
	 * @see {@link org.repaso.producto.Envase#Envase(double, double, int)}
	 */
	public static void agregar(String etiqueta, Envase envase) {
		envases.put(etiqueta, envase);
	}
	
	/**
	 * 
	 */
	@Deprecated
	public static void listarEtiquetas() {
		for (Map.Entry<String, Envase> entry : envases.entrySet()) {
			System.out.println("Etiqueta: " + entry.getKey() + ", envase: " + entry.getValue().toString());
		}
	}
	
	/**
	 * <p>Obtiene un envase partiendo de la etiqueta que lo identifica en el mapa</p>
	 * @param etiqueta	&emsp;(<b>String</b>) etiqueta identificadora
	 * @return Envase
	 * @throws InvalidEnvaseException
	 */
	public static Envase obtener(String etiqueta) throws InvalidEnvaseException {
		
		for (Map.Entry<String, Envase> entry : envases.entrySet()) {
			if(entry.getKey().compareTo(etiqueta) == 0) {
				return entry.getValue();
			}
		}
		
		throw new InvalidEnvaseException(etiqueta);
	}
	
	/**
	 * Retorna un envase aleatorio, recorriendo el <b>HashMap</b> de envases cargados en el programa
	 * @return {@link org.repaso.producto.Envase}
	 * @throws InvalidEnvaseException
	 */
	public static Envase getAleatorio() throws InvalidEnvaseException {
		int aleatorio = ThreadLocalRandom.current().nextInt(0, envases.size());
		int actual = 0;
		
		for (Map.Entry<String, Envase> entry : envases.entrySet()) {
			if(aleatorio != actual) {
				actual++;
			} else {
				return entry.getValue();
			}
		}
		throw new InvalidEnvaseException("aleatorio");
	}

}

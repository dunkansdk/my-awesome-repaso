package org.repaso.heladeria;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONArray;
import org.json.JSONObject;
import org.repaso.excepciones.InvalidEnvaseException;
import org.repaso.producto.Envase;
import org.repaso.producto.Helado;
import org.repaso.ventas.Item;
import org.repaso.ventas.Venta;

/**
 * <p>Controlador de las Ventas</p>
 * @see {@link org.repaso.ventas.Venta}
 */
public class VentaController {
	
	private static ArrayList<Venta> ventas;
	
	public static void init() {
		ventas = new ArrayList<Venta>();
	}
	
	public static void vender(Venta venta) {
		ventas.add(venta);
	}
	
	/**
	 * <p>Lista las ventas actuales</p>
	 */
	public static void listar() {
		for(Venta venta : ventas) {
			System.out.println(venta);
		}
	}
	
	/**
	 * <p>Suma las ventas realizadas y las retorna</p>
	 * @return dobule Ganancias totales de las ventas
	 */
	public static double getGanancias() {
		double ganancias = 0;
		for(Venta venta : ventas) {
			ganancias += venta.getTotal();
		}
		return ganancias;
	}
	
	/**
	 * <p>Simula la venta a un cliente a partir de datos aleatorios</p>
	 * {@link org.repaso.ventas.Venta}
	 * {@link org.repaso.heladeria.EnvaseController#getAleatorio()}
	 * @throws InvalidEnvaseException
	 */
	public static void simular() throws InvalidEnvaseException {
		
		int envases = ThreadLocalRandom.current().nextInt(1, 4);
		ArrayList<Item> items = new ArrayList<Item>();
		
		for(int i = 0; i < envases; ++i) {	
			ArrayList<Integer> gustos = new ArrayList<Integer>();
			Envase envase = EnvaseController.getAleatorio();
			
			// Obtenemos la cantidad de gustos que va a tener el envase
			int cantidadGustos = ThreadLocalRandom.current().nextInt(1, envase.getCantidad());
			
			for(int j = 0; j < cantidadGustos; j++) {
				// Obtenemos los gustos que va a tener el envase
				int gustornd = ThreadLocalRandom.current().nextInt(1, 5);
				gustos.add(gustornd);
			}
			
			items.add(new Item(envase, gustos.toArray(new Integer[gustos.size()])));			
		}		
		
		vender(new Venta(items.toArray(new Item[items.size()])));

	}
	
	/**
	 * <p>Genera un archivo binario con el importe de cada venta</p>
	 */
	public static void generarMontoVenta()
	{
		// Creamos el archivo File
		FileOutputStream file;
		try {
			file = new FileOutputStream("resources/ventas.dat");
			DataOutputStream data = new DataOutputStream(file);
			
			// Escribimos
			for(Venta venta : ventas)
				data.writeDouble(venta.getTotal());
							
			data.close();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	/**
	 * <p>Lee el archivo de montos de venta</p>
	 */
	public static void leerMontoVenta()
	{
		FileInputStream file;
		try {
			file = new FileInputStream("resources/ventas.dat");
			DataInputStream data = new DataInputStream(file);
			
			// Leemos
			while(data.available() > 0)
				System.out.println("Venta: $" + data.readDouble());
			
			data.close();
			file.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

package org.repaso;

import java.util.HashMap;

import org.repaso.excepciones.InvalidEnvaseException;
import org.repaso.heladeria.EnvaseController;
import org.repaso.heladeria.VentaController;
import org.repaso.producto.Envase;
import org.repaso.producto.Helado;
import org.repaso.stock.Stock;

public class Main {

	public static void main(String[] args) {
		
		EnvaseController.init();
		Stock.init();
		VentaController.init();
		
		EnvaseController.agregar("Cuarto", new Envase(100, 0.250, 2));
		EnvaseController.agregar("Medio", new Envase(200, 0.5, 3));
		EnvaseController.agregar("Kilo", new Envase(300, 1, 4));
		EnvaseController.agregar("Cono", new Envase(50, 0.100, 2));
		EnvaseController.agregar("Vaso", new Envase(25, 0.150, 2));
		
		HashMap<String, Integer> receta = new HashMap<String, Integer>();
		
		receta = new HashMap<String, Integer>();
		receta.put("Azucar", 8);
		receta.put("Frutilla", 5);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Stock.agregar(1, new Helado("Frutilla", 5.3, receta));
		
		receta = new HashMap<String, Integer>();
		receta.put("Azucar", 8);
		receta.put("Chocolate", 8);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Stock.agregar(2, new Helado("Chocolate", 2.3, receta));
		
		receta = new HashMap<String, Integer>();
		receta.put("Azucar", 8);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Stock.agregar(3, new Helado("Crema del cielo", 7.1, receta));
		
		receta = new HashMap<String, Integer>();
		receta.put("Menta", 4);
		receta.put("Azcar", 8);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Stock.agregar(4, new Helado("Menta", 1.8, receta));
		
		receta = new HashMap<String, Integer>();
		receta.put("Azucar", 8);
		receta.put("Crema", 2);
		receta.put("Dulce", 2);
		receta.put("Leche", 10);
		Stock.agregar(5, new Helado("Dulce de leche", 4.2, receta));
		
		receta = new HashMap<String, Integer>();
		receta.put("Azucar", 8);
		receta.put("Crema", 2);
		receta.put("Leche", 10);
		Stock.agregar(5, new Helado("Vainilla", 9.2, receta));
		
		/*
		try {
			VentaController.vender(new Venta(	new Item(EnvaseController.obtener("Cono"), 2, 3)
												));
			
			VentaController.vender(new Venta(	new Item(EnvaseController.obtener("Kilo"), 1, 5, 2), 
												new Item(EnvaseController.obtener("Medio"), 2, 3)
												));
			
		} catch (InvalidEnvaseException e) {
			e.printStackTrace();
		}
		*/
		
		try {
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
			VentaController.simular();
		} catch (InvalidEnvaseException e) {
			e.printStackTrace();
		}

		VentaController.listar();
		System.out.println("Gustos fuera de Stock: " + Stock.listar(1)); // Menos de 1KG de helado
		System.out.println("Las ganancias fueron: " + VentaController.getGanancias());
		
		/* Se testeo, funciona, no sirve para nada btw
			VentaController.generarMontoVenta();
			VentaController.leerMontoVenta();		
			System.out.println(Stock.exportarJSON().toString());
		*/
	}

}

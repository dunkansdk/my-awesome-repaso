package org.repaso.producto;

/**
 * <p>Envase donde se va a servir el helado para vender</p>
 * 
 */
public class Envase {

	private double precio;
	private double peso;
	private int cantidad;
	
	/**
	 * <p>Constructor de un envase</p>
	 * @param precio	&emsp;(<b>Double</b>) precio correspondiente al envase
	 * @param peso		&emsp;(<b>Double</b>) peso correspondiente al envase
	 * @param cantidad	&emsp;(<b>Double</b>) cantidad de gustos que se pueden agregar al envase
	 */
	public Envase(double precio, double peso, int cantidad) {
		this.precio = precio;
		this.peso = peso;
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public int getCantidad() {
		return cantidad;
	}
	
	@Override
	public String toString() {
		return "Precio: " + precio + ", peso: " + peso;
	}

}

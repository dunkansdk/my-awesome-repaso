package org.repaso.producto;

import java.util.HashMap;

/**
 * <p>Es un sabor de helado en la heladeria, con la cantidad y la receta correspondiente</p>
 *
 */
public class Helado {
	
	private String nombre;
	private HashMap<String, Integer> receta;
	private double cantidad;
	
	/**
	 * Constructor de un gusto de helado
	 * @param nombre	&emsp;(<b>Integer</b>) nombre que idenfica el sabor
	 * @param cantidad	&emsp;(<b>Integer</b>) cantidad de KG disponibles de ese gusto
	 * @param receta	&emsp;(<b>HashMap<String, Integer></b>) receta empleada para la elaboracion <b>(Ingrediente, Cantidad)</b>
	 */
	public Helado(String nombre, double cantidad, HashMap<String, Integer> receta) {
		this.nombre = nombre;
		this.cantidad = cantidad;		
		this.receta = receta;
	}

	public String getNombre() {
		return nombre;
	}
	
	public double getCantidad() {
		return cantidad;
	}
	
	public HashMap<String, Integer> getReceta() {
		return receta;
	}
	
	/**
	 * Quita la cantidad de healdo vendido del Stock
	 * @param cantidad
	 */
	public void vender(double cantidad) {
		this.cantidad -= cantidad;
	}

	@Override
	public String toString() {
		return "Helado [nombre=" + nombre + ", cantidad=" + cantidad + "]";
	}
	
}
